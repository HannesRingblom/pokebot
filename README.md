# Assignment 4 - Pokebot
A program that farms Pokemon. 

## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [Members' tasks](#members-tasks)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Requirements

Requires `gcc`, `cmake` and `VSCode`, Python 3.

## Setup
Clone the git repo for the Python Generic DB Server (https://gitlab.com/noroff-accelerate/embedded-bootcamp/projects/python-generic-db-rest-server). Run the server with the command

```sh
$ cd ~/python-generic-db-rest-server
$ python3 src/main.py
```

Clone the git repo and start VSCode in the Pokebot root folder. Use the following instructions to build and run:

    ```sh
    # Press F1 and select CMake:Build.
    $ cd build
    $ ./pokebot
    ```

For both contributers, the program was automated with the output saved to a txt.file using the following commands:

    ```sh
    $ crontab -e
    # Add the following to the bottom of the file:
    $ * * * * * ~/pokebot/build/pokebot
    $ */5 * * * * curl localhost:5000/fetch/all > ~/pokebot/pokemon_caught.txt
    ```

## Members' tasks


## Maintainers

[Hannes @HannesRingblom](https://gitlab.com/HannesRingblom)

## Contributors and License
Copyright 2022,

[Hannes Ringblom @HannesRingblom](https://gitlab.com/HannesRingblom)
[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

