#include <iostream>
#include <string>
#include "restclient-cpp/restclient.h"
#include "restclient-cpp/connection.h"
#include <nlohmann/json.hpp>
#include <cstdlib>

using json = nlohmann::json;

int main(int, char**) {
    std::cout << "Starting rest-helloworld\n";
    std::string url= "https://pokeapi.co/api/v2/pokemon/";

    int random_pokemon = 1;
    srand((unsigned)time(NULL));
    random_pokemon = (rand() % 898) +1;
    std::string new_url = url + std::to_string(random_pokemon);
    RestClient::Response r = RestClient::get(new_url);

    auto json_body = json::parse(r.body);
    std::cout<<json_body["name"]<<std::endl;
    for (size_t i = 0; i < json_body["types"].size(); i++)
    {
        std::cout<<json_body["types"][i]["type"]["name"]<<std::endl;
    }

    RestClient::Connection* conn = new RestClient::Connection("http://127.0.0.1:5000");
    RestClient::Response all = RestClient::get("http://127.0.0.1:5000/fetch/all");

    auto json_all = json::parse(all.body);
    bool exists = false;
    if (!json_all.empty()){
        for (auto pokemon : json_all/*["data"]*/)
        {
            std::string poke_data = pokemon["data"];
            std::string name = "";
            for (size_t i = 0; i < poke_data.length(); i++)
            {
                if (poke_data[i] != ',')
                {
                    name.push_back(poke_data[i]);
                }else{
                    break;
                }
            }
            if (name == json_body["name"])
            {
                exists = true;
                break;
            }
        }
    }
    if (!exists){
        // instead, you could also write (which looks very similar to the JSON above)
        json json_pokemon;
        if (json_body["types"].size() == 1)
        {
            std::string name = json_body["name"];
            std::string sprite = json_body["sprites"]["front_default"];
            std::string type1 = json_body["types"][0]["type"]["name"];
            std::string pokemon_data = "";
            pokemon_data.append(name + ", ");
            pokemon_data.append(type1 + ", ");
            pokemon_data.append(sprite);
            json_pokemon["data"] = pokemon_data;  
            json_pokemon["meta"] = "pokemon";
        }else{
            
           
            std::string name = json_body["name"];
            std::string sprite = json_body["sprites"]["front_default"];
            std::string type1 = json_body["types"][0]["type"]["name"];
            std::string type2 = json_body["types"][1]["type"]["name"];
            std::string pokemon_data = "";
            pokemon_data.append(name + ", ");
            pokemon_data.append(type1 + ", ");
            pokemon_data.append(type2 + ", ");
            pokemon_data.append(sprite);

            json_pokemon["data"] = pokemon_data;  
            json_pokemon["meta"] = "pokemon";
        }
        conn->AppendHeader("Content-Type","application/json");
        std::string poke_string = json_pokemon.dump();
        std::cout<<poke_string<<std::endl;
        RestClient::Response resp = conn->post("/submit", poke_string);
        std::cout<<resp.body<<std::endl;
    }
}